#!/bin/bash

SCRNAME=get_logs.sh

#INSTRUCTION: WORK FOR ALL TEST CASES.

#What you need:
#1. A linux machine
#2. A USB Hub

#How to grab logs from stability tests:
#1. Assume you run the whole test suits (ThermalValidation, Power On/Off, S/R, Battery Drain...etc)
#2. Example you have 1 fail in Battery Drain, 2 fail in S/R...etc.
#3. Label each device according to the test it failed (e.g Device "A" failed in Battery Drain)
#4. Separate between WFO and WAN failed devices
#5. Plug all devices to the USB hub
#6a.On WAN devices:
#   $ get_logs.sh "Suspend Resume WAN ON" -B -W -a

#6b.On WFO devices:
#   $ get_logs.sh "Suspend Resume" -B -a

#- The log will create a folder name "Suspend Resume" and in side that folder will be individual FSN and logs of the device.
#===========================================================================================================================

#2015-07-13 Version 0.70
VER="0.70"
ADB="adb"
ARCH="MTK-MT8163"

EXITFLAG=0
BASELOGPATH=/data/debug_service

trap "EXITFLAG=1" SIGINT SIGQUIT

function arg_parser
{
	OPT_HELP=0;
	OPT_START_LOGS=0;
	OPT_CLEAN_LOGS="";
	OPT_MANUAL_DM=0;
	OPT_NOKINDLE_LOG=0;
	OPT_ARCHIVE=0;
	OPT_BUILD_NUM=0;
	OPT_WAN=0;
	OPT_TEST_CASE="";
	OPT_CHK_BATTERY="";
	OPT_CHK_DMESG="";
	OPT_CHK_KINDLELOG="";
	OPT_CONFIG_NAME="";
	OPT_DEVICE_NUM="";

	OPT_SCRIPT_DIR="$(cd $(dirname $0) && pwd)"
	while true; do
	  if [ "$1" == "" ]; then break; fi;
	  case "$1" in
	  -h|--help) OPT_HELP=1;;
	  -l|--logs) OPT_START_LOGS=1;;
	  -z|--clean-logs) OPT_CLEAN_LOGS=1;;
	  -m|--manual_dm) OPT_MANUAL_DM=1;;
	  -N|--nokindlelog) OPT_NOKINDLE_LOG=1;;
	  -a|--archive) OPT_ARCHIVE=1;;
	  -B|--build) OPT_BUILD_NUM=1;;
	  -W|--wan) OPT_WAN=1;;
	  -b|--battery) shift; OPT_CHK_BATTERY=$1;;
	  -d|--dmesg) shift; OPT_CHK_DMESG=$1;;
	  -k|--kindlelog) shift; OPT_CHK_KINDLELOG=$1;;
	  -s|--suspend) shift; OPT_CHK_DMESG_SUSPEND=$1;;
	  -c|--config) shift; OPT_CONFIG_NAME=$1;;
	  -D|--Device) shift; OPT_DEVICE_NUM=$1;;
	  -*);;
	  *) OPT_TEST_CASE=$1;
	  esac;
	  shift;
	done;
}

# RUN_MOD:
#   0 - help
#   1 - Get logs
#   2 - Analyze battery log
#   3 - Analyze dmesg log for common problems
#   4 - Analyze Kindle log
#   5 - Analyze dmesg log for suspend time
function arg_check
{
	RUN_MODE=0;

	if [ $OPT_HELP -eq 1 ]; then return; fi;
	if [ $OPT_START_LOGS -eq 1 ]; then RUN_MODE=6; return; fi;
	if [ "$OPT_TEST_CASE" != "" ]; then RUN_MODE=1; return; fi;
	if [ -f "$OPT_CHK_BATTERY" ]; then RUN_MODE=2; return; fi;
	if [ -f "$OPT_CHK_DMESG" ]; then RUN_MODE=3; return; fi;
	if [ -f "$OPT_CHK_KINDLELOG" ]; then RUN_MODE=4; return; fi;
	if [ -f "$OPT_CHK_DMESG_SUSPEND" ]; then RUN_MODE=5; return; fi;
}

function mode0_help
{
	echo "Usage: ${SCRNAME} [OPTIONS] {name_of_test_case}"
	echo "Version: $VER"
	echo "Options:"
	echo "   -h, --help             - Show this help"
	echo "   -m, --manual_dm        - Skip internal executing \";dm\""
	echo "   -N, --nokindlelog      - No wait for KindleLog"
	echo "   -b, --battery    file  - Analyze of battery log"
	echo "   -d, --dmesg      file  - Analyze of dmesg log"
	echo "   -k, --kindlelog  file  - Analyze of Kindle log"
	echo "   -s, --suspend    file  - Make S/R table from dmesg log"
	echo "   -c, --config     str   - Configuration name"
	echo "   -B, --build            - If you need build number to be part of folder name"
	echo "   -W, --wan              - If you need Modem logs to be pulled out (WAN devices only)"
	echo "   -a, --archive          - Auto make ZIP file of results"
	echo "   -l, --logs             - Permanently set property "y" for all log services and starting"
	echo "   -z, --clean-logs       - Delete local log files after retrieving from the device"
	echo "   -D, --Device     ser.  - Get logs only from specific device"
	echo ""
	echo "Example of case for getting logs"
	echo "   $ ${SCRNAME} \"Suspend Resume\" -B -a -c \"Config A\""
	echo "   $ ${SCRNAME} \"Suspend Resume\" -m -B -a -c \"Config A\""
	echo ""
	echo "   $ ${SCRNAME} \"Suspend Resume WAN ON\" -B -W -a -c \"Config A\""
	echo "   $ ${SCRNAME} \"Suspend Resume WAN ON\" -m -W -B -a -c \"Config A\""
	echo ""
	echo "   Folder structure:"
	echo "   <date>_<time>_<My custom test case>"
	echo "         |--- README"
	echo "         +--- <device id #0>_<date>_<time>_<image build number>_<My custom test case>_<configuration name>"
	echo "         |          |-- README"
	echo "         |          |-- log files"
	echo "         |          |"
	echo "         |"
	echo "         +--- <device id #1>_<date>_<time>_<image build number>_<My custom test case>_<configuration name>"
	echo "         |"
	echo "         +--- <device id #2>_<date>_<time>_<image build number>_<My custom test case>_<configuration name>"
	echo "         |"
	echo ""
	echo "Example of case for getting & cleaning logs"
	echo "   $ ${SCRNAME} \"Suspend Resume\" -a -z"
	echo ""
	echo "Example to analyze battery log"
	echo "   $ ${SCRNAME} -b battery.log"
	echo ""
	echo "Example to analyze dmesg log"
	echo "   $ ${SCRNAME} -d dmesg_full.log"
	echo "   $ ${SCRNAME} -d dmesg_current.log"
	echo "   $ ${SCRNAME} -d last_kmsg.log"
	echo ""
	echo "Example to analyze Kindle log"
	echo "   $ ${SCRNAME} -k KindleLogs.D0260503223200EK.12.06.14.02.04.58.sv0.p0.a0.det"
	echo ""
	echo "Example to make summary table of suspend/resume from kernel logs"
	echo "   $ ${SCRNAME} -s dmesg_full.log"
	echo "   $ ${SCRNAME} -s dmesg_current.log"
	echo "   $ ${SCRNAME} -s last_kmsg.log"
	echo ""
	exit -1;
}

function battery_analyze
{
    cat $1 | grep "^[0-9]" | awk \
    'BEGIN {
        FS = ",";
        count = 0;
        prev_time = 0;
        prev_stime = 0;
        prev_cap = 0;
        first_time = 0;
        first_stime = 0;
        first_cap = 0;
        cap_min=100;
        cap_max=0;

        printf("+----+----------+-------------------------------------+-------------------------------------+\n");
        printf("|    |          |                L o c a l            |                T o t a l            |\n");
        printf("| No |  T I M E +--------+------+-------+-----+-------+--------+------+-------+-----+-------+\n");
        printf("|    |          |Duration|Resume|Suspend|Drain|  Step |Duration|Resume|Suspend|Drain|  Step |\n");
        printf("|    |   [sec]  |  [sec] | [%%]  |  [%%]  | [%%] | [%%/h] |  [sec] | [%%]  |  [%%]  | [%%] | [%%/h] |\n");
        printf("+----+----------+--------+------+-------+-----+-------+--------+------+-------+-----+-------+\n");
    }
    {
        if (index($0, "#") > 0) {
            next;
        }

        if ($1 != 0) {
            count += 1;
            curr_time=$1;
            curr_stime=$2;
            curr_cap=$6;

            if (first_time == 0) {
                first_time = curr_time;
                first_stime = curr_stime;
                first_cap = curr_cap;
            }

            if (curr_cap > cap_max) {
              cap_max = curr_cap;
            }

            if (curr_cap < cap_min) {
              cap_min = curr_cap;
            }

            if (count > 1 && prev_time < curr_time) {
                loc_duration = curr_time - prev_time;
                if (loc_duration < 1) {
                    next;
                }

                loc_suspend = curr_stime - prev_stime;
                if (loc_suspend > loc_duration)
                    loc_suspend = loc_duration;
                loc_resume = loc_duration - loc_suspend;
                loc_resume_per = (loc_resume * 100) / loc_duration;
                loc_suspend_per = (loc_suspend * 100) / loc_duration;
                loc_drain = curr_cap - prev_cap;
                loc_step = (loca_drain * 60 * 60) / loc_duration;

                tot_duration = curr_time - first_time;
                tot_suspend = curr_stime - first_stime;
                tot_resume = tot_duration - tot_suspend;
                tot_resume_per = (tot_resume * 100) / tot_duration;
                tot_suspend_per = (tot_suspend * 100) / tot_duration;
                tot_drain = curr_cap - first_cap;
                tot_step = (tot_drain * 60 * 60) / tot_duration;
                printf("|%4d|%10d| %6d |  %3d |  %3d  | %3d |%7.3f|%8d|  %3d |  %3d  | %3d |%7.3f|\n",
                    count-1, curr_time,
                    loc_duration, loc_resume_per, loc_suspend_per, loc_drain, loc_step,
                    tot_duration, tot_resume_per, tot_suspend_per, tot_drain, tot_step);
            }

            prev_time = curr_time;
            prev_stime = curr_stime;
            prev_cap = curr_cap;
        }
    }
    END {
        printf("+----+----------+--------+------+-------+-----+-------+--------+------+-------+-----+-------+\n");
        if (tot_duration > 0) {
            tot_h = int(tot_duration / 60 / 60);
            tot_m = int((tot_duration - tot_h * 60 * 60) / 60);
            tot_s = tot_duration - tot_h * 60 * 60 - tot_m * 60;
            tot_drain = cap_max - cap_min;
            tot_step = (tot_drain * 60 * 60) / tot_duration;
            printf("Total:\n");
            printf("  Time:     %7d sec (%dh %dm %ds)\n", tot_duration, tot_h, tot_m, tot_s);
            printf("  Resume:   %7d sec / %7.3f %%\n", tot_resume, tot_resume_per);
            printf("  Suspend:  %7d sec / %7.3f %%\n", tot_suspend, tot_suspend_per);
            printf("  Drain:    %7.2f %%   / %7.3f %%/h\n", tot_drain, tot_step);
            printf("Capacity:\n");
            printf("  Max:      %7d %%\n", cap_max);
            printf("  Min:      %7d %%\n", cap_min);
            printf("  Diff:     %7d %%\n", cap_max - cap_min);
            printf("Summary:|%d|%d|%d|%.2f|%.2f|%.2f|%.3f|\n",
                tot_duration, tot_resume, tot_suspend,
                tot_resume_per, tot_suspend_per, tot_drain, tot_step);
        } else {
            printf("Missing data or wrong format!!!\n");
        }
    }'
}

# $1 - Filename to count EMI MPU violations
function dmesg_chk_cnt_emi_mpu_violations
{
	cnt_emi_mpu_violations=`grep "Fail to clear EMI MPU violation" $1 --count 2>/dev/null`
	[ "$cnt_emi_mpu_violations" == "" ] && cnt_emi_mpu_violations=0
}

declare KNOWN_KERNELTRAPS
KNOWN_KERNELTRAPS[0]="WARNING: at "
KNOWN_KERNELTRAPS[1]="BUG: failure at "
KNOWN_KERNELTRAPS[2]="Oops:"
KNOWN_KERNELTRAPS[3]="Oops - BUG"
KNOWN_KERNELTRAPS[4]="Oops - undefined instruction"
KNOWN_KERNELTRAPS[5]="Oops - bad mode"
KNOWN_KERNELTRAPS[6]="Oops - bad syscall"
KNOWN_KERNELTRAPS[7]="Oops - bad syscall(2)"

# $1 - Filename to count all Kernel traps
function dmesg_chk_cnt_kernel_traps
{
	for msg in "${KNOWN_KERNELTRAPS[@]}"; do
		count=`grep "$msg" $1 --count  2>/dev/null`
		[ "$count" == "" ] && count=0
		cnt_kernel_traps=$(($cnt_kernel_traps+$count))
	done;
}

# $1 - Filename to show all Kernel traps
function dmesg_chk_show_kernel_traps
{
	for msg in "${KNOWN_KERNELTRAPS[@]}"; do
		count=`grep "${msg}" $1 --count 2>/dev/null`
		[ "$count" == "" ] && count=0
		[ $count -eq 0 ] && continue;
		printf "  |\t    %-48s %d\n" "$msg" "$count"
	done;
}

declare KNOWN_CRYPTOERROR
KNOWN_CRYPTOERROR[0]="Device lookup failed"
KNOWN_CRYPTOERROR[1]="error adding target"

# $1 - Filename to count all Crypto errors
function dmesg_chk_cnt_crypto_error
{
	for msg in "${KNOWN_CRYPTOERROR[@]}"; do
		count=`grep "$msg" $1 --count 2>/dev/null`
		[ "$count" == "" ] && count=0
		cnt_crypto_error=$(($cnt_crypto_error+$count))
	done;
}

# $1 - Filename to show all Crypto errors
function dmesg_chk_show_crypto_error
{
	for msg in "${KNOWN_CRYPTOERROR[@]}"; do
		count=`grep "${msg}" $1 --count 2>/dev/null`
		[ "$count" == "" ] && count=0
		[ $count -eq 0 ] && continue;
		printf "  |\t    %-48s %d\n" "$msg" "$count"
	done;
}

declare KNOWN_FSERROR
KNOWN_FSERROR[0]="EXT4-fs\ error"
KNOWN_FSERROR[1]="Aborting\ journal\ on\ device"
KNOWN_FSERROR[2]="Remounting\ filesystem\ read-only"

# $1 - Filename to count all FS errors
function dmesg_chk_cnt_fs_error
{
	for msg in "${KNOWN_FSERROR[@]}"; do
		count=`grep "$msg" $1 --count 2>/dev/null`
		[ "$count" == "" ] && count=0
		cnt_fs_error=$(($cnt_fs_error+$count))
	done;
}

# $1 - Filename to show all FS errors
function dmesg_chk_show_fs_error
{
	for msg in "${KNOWN_FSERROR[@]}"; do
		count=`grep "${msg}" $1 --count 2>/dev/null`
		[ "$count" == "" ] && count=0
		[ $count -eq 0 ] && continue;
		printf "  |\t    %-48s %d\n" "$msg" "$count"
	done;
}

KNOWN_FUNC=" KREE_TeeServiceCall"
KNOWN_FUNC+=" KREE_TeeServiceCallNoCheck"
KNOWN_FUNC+=" MISRWrapper"
KNOWN_FUNC+=" OSDumpStack"
KNOWN_FUNC+=" PVRSRVDebugRequest"
KNOWN_FUNC+=" PVRSRVDriverShutdown"
KNOWN_FUNC+=" PVRSRVSetPowerStateKM"
KNOWN_FUNC+=" RGXCheckFWActivePowerState"
KNOWN_FUNC+=" RGX_MISRHandler"
KNOWN_FUNC+=" SyS_access"
KNOWN_FUNC+=" SyS_faccessat"
KNOWN_FUNC+=" SyS_inotify_add_watch"
KNOWN_FUNC+=" SyS_ioctl"
KNOWN_FUNC+=" SyS_reboot"
KNOWN_FUNC+=" __dabt_svc"
KNOWN_FUNC+=" __do_kernel_fault"
KNOWN_FUNC+=" __do_softirq"
KNOWN_FUNC+=" __fuse_request_send"
KNOWN_FUNC+=" __irq_svc"
KNOWN_FUNC+=" __lock_acquire"
KNOWN_FUNC+=" __lookup_hash"
KNOWN_FUNC+=" __m4u_dealloc_mva"
KNOWN_FUNC+=" __mmc_claim_host"
KNOWN_FUNC+=" __refrigerator"
KNOWN_FUNC+=" __schedule"
KNOWN_FUNC+=" __schedule_bug"
KNOWN_FUNC+=" __und_svc_finish"
KNOWN_FUNC+=" __xlog_output"
KNOWN_FUNC+=" __xlog_printk"
KNOWN_FUNC+=" _ion_buffer_destroy"
KNOWN_FUNC+=" _raw_spin_lock"
KNOWN_FUNC+=" _raw_spin_lock_irqsave"
KNOWN_FUNC+=" _raw_spin_lock_nested"
KNOWN_FUNC+=" _raw_spin_unlock_irq"
KNOWN_FUNC+=" _raw_spin_unlock_irqrestore"
KNOWN_FUNC+=" add_wait_queue"
KNOWN_FUNC+=" aee_wdt_fiq_info"
KNOWN_FUNC+=" aee_wdt_irq_info"
KNOWN_FUNC+=" arch_cpu_idle"
KNOWN_FUNC+=" arch_idle"
KNOWN_FUNC+=" arch_trigger_all_cpu_backtrace"
KNOWN_FUNC+=" arm_notify_die"
KNOWN_FUNC+=" cpu_startup_entry"
KNOWN_FUNC+=" debug_locks_off"
KNOWN_FUNC+=" device_shutdown"
KNOWN_FUNC+=" die"
KNOWN_FUNC+=" disp_ovl_engine_kthread"
KNOWN_FUNC+=" do_DataAbort"
KNOWN_FUNC+=" do_page_fault"
KNOWN_FUNC+=" do_raw_spin_lock"
KNOWN_FUNC+=" do_softirq"
KNOWN_FUNC+=" do_undefinstr"
KNOWN_FUNC+=" do_vfs_ioctl"
KNOWN_FUNC+=" dump_backtrace"
KNOWN_FUNC+=" dump_stack"
KNOWN_FUNC+=" filename_lookup"
KNOWN_FUNC+=" fuse_lookup"
KNOWN_FUNC+=" fuse_lookup_name"
KNOWN_FUNC+=" fuse_request_send"
KNOWN_FUNC+=" get_online_cpus"
KNOWN_FUNC+=" gic_handle_irq"
KNOWN_FUNC+=" go_to_rgidle"
KNOWN_FUNC+=" handle_IPI"
KNOWN_FUNC+=" handle_IRQ"
KNOWN_FUNC+=" hmp_force_down_migration"
KNOWN_FUNC+=" idle_balance"
KNOWN_FUNC+=" ion_buffer_destroy"
KNOWN_FUNC+=" ion_buffer_put"
KNOWN_FUNC+=" ion_free"
KNOWN_FUNC+=" ion_handle_destroy"
KNOWN_FUNC+=" ion_handle_put"
KNOWN_FUNC+=" ion_ioctl"
KNOWN_FUNC+=" ion_mm_heap_free"
KNOWN_FUNC+=" ion_mm_heap_free_bufferInfo"
KNOWN_FUNC+=" irq_exit"
KNOWN_FUNC+=" kernel_power_off"
KNOWN_FUNC+=" kthread"
KNOWN_FUNC+=" kwdt_thread"
KNOWN_FUNC+=" link_path_walk"
KNOWN_FUNC+=" load_balance"
KNOWN_FUNC+=" lock_acquire"
KNOWN_FUNC+=" lookup_real"
KNOWN_FUNC+=" lookup_slow"
KNOWN_FUNC+=" m4u_dealloc_mva_sg"
KNOWN_FUNC+=" m4u_invalid_seq_range_by_mva"
KNOWN_FUNC+=" m4u_invalid_tlb"
KNOWN_FUNC+=" m4u_tee_isr_handle"
KNOWN_FUNC+=" msleep"
KNOWN_FUNC+=" mt_post_schedule"
KNOWN_FUNC+=" mtk_wcn_hif_sdio_wmt_control"
KNOWN_FUNC+=" mutex_lock_nested"
KNOWN_FUNC+=" oops_enter"
KNOWN_FUNC+=" opfunc_func_off"
KNOWN_FUNC+=" path_lookupat"
KNOWN_FUNC+=" platform_drv_shutdown"
KNOWN_FUNC+=" printk"
KNOWN_FUNC+=" process_one_work"
KNOWN_FUNC+=" rcu_gp_kthread"
KNOWN_FUNC+=" rest_init"
KNOWN_FUNC+=" ret_fast_syscall"
KNOWN_FUNC+=" ret_from_fork"
KNOWN_FUNC+=" rgidle_handler"
KNOWN_FUNC+=" run_rebalance_domains"
KNOWN_FUNC+=" sched_getaffinity"
KNOWN_FUNC+=" schedule"
KNOWN_FUNC+=" schedule_preempt_disabled"
KNOWN_FUNC+=" schedule_timeout"
KNOWN_FUNC+=" schedule_timeout_uninterruptible"
KNOWN_FUNC+=" sdio_claim_host"
KNOWN_FUNC+=" secondary_start_kernel"
KNOWN_FUNC+=" show_regs"
KNOWN_FUNC+=" smp_send_all_cpu_backtrace"
KNOWN_FUNC+=" spin_bug"
KNOWN_FUNC+=" spin_dump"
KNOWN_FUNC+=" start_kernel"
KNOWN_FUNC+=" user_path_at"
KNOWN_FUNC+=" user_path_at_empty"
KNOWN_FUNC+=" vprintk_emit"
KNOWN_FUNC+=" wmt_core_ctrl"
KNOWN_FUNC+=" wmt_core_opid"
KNOWN_FUNC+=" wmt_core_opid_handler"
KNOWN_FUNC+=" wmt_ctrl"
KNOWN_FUNC+=" wmt_ctrl_sdio_func"
KNOWN_FUNC+=" wmt_func_wifi_ctrl"
KNOWN_FUNC+=" wmt_func_wifi_off"
KNOWN_FUNC+=" wmtd_thread"
KNOWN_FUNC+=" worker_thread"

FLT_BACKTRACE="\[<[0-9abcdef]{8}>\] \([0-9a-zA-Z_]*+"

# $1 - Filename to count all backtrace
function dmesg_chk_cnt_backtrace
{
	cnt_back_trace=`grep -E "$FLT_BACKTRACE" $1 --count 2>/dev/null`
	[ "$cnt_back_trace" == "" ] && cnt_back_trace=0
}

# $1 - Filename to show all backtrace
function dmesg_chk_show_backtrace
{
	for func in $KNOWN_FUNC; do
		count=`grep -E "$FLT_BACKTRACE" $1 | grep "${func}+" --count 2>/dev/null`
		[ "$count" == "" ] && count=0
		[ $count -eq 0 ] && continue;
		printf "  |\t    %-48s %d\n" "$func" "$count"
	done;
}

# $1 - Base folder for recursive check
function dmesg_chk_find_log_folders
{
        LOG_FOLDERS=`find $1 -type f -name "README" | grep -E "[0-9A-Z]{16}_20[0-9]{6}_[0-9]{4}" | sed -e "s/\/README//" | sort -u`
}

# $1 - Base folder for recursive check
function dmesg_chk_find_log_files
{
	LOG_FILES=`find $1 -name "dmesg*.log" -or -name "last_kmsg*.log" -or -name "SYSTEM_LAST_KMSG*" | sed -e "s/\.\///" | sort -u`
}

# $1 - Base folder for recursive check
function dmesg_summary_folder
{
	SHOW_FOLDER=0
	CURDIR=`pwd`
	cd $1
	if [ $? -ne 0 ]; then
		cd $CURDIR;
		return -1;
	fi;

	dmesg_chk_find_log_files .
	[ -z "$LOG_FILES" ] && return 1

	for file in $LOG_FILES; do
		cnt_kernel_traps=0
		cnt_back_trace=0
		cnt_fs_error=0
		cnt_crypto_error=0
		cnt_emi_mpu_violations=0

		dmesg_chk_cnt_kernel_traps $file
		dmesg_chk_cnt_backtrace $file
		dmesg_chk_cnt_fs_error $file
		dmesg_chk_cnt_crypto_error $file
		dmesg_chk_cnt_emi_mpu_violations $file

		[ $cnt_back_trace -eq 0 ] && [ $cnt_fs_error -eq 0 ] && \
		[ $cnt_emi_mpu_violations -eq 0 ] && [ $cnt_crypto_error -eq 0 ] && \
		[ $cnt_kernel_traps -eq 0 ] && continue;

		if [ $SHOW_FOLDER -eq 0 ]; then echo "$1"; SHOW_FOLDER=1; fi;
		echo "  |"
		echo "  +-- $file"
		[ $cnt_emi_mpu_violations -ne 0 ] && echo -e "  |\t* EMI MPU Violations: $cnt_emi_mpu_violations"
		[ $cnt_fs_error -ne 0 ] && (echo -e "  |\t* FS Errors: $cnt_fs_error"; dmesg_chk_show_fs_error $file)
		[ $cnt_crypto_error -ne 0 ] && (echo -e "  |\t* Crypto Errors: $cnt_crypto_error"; dmesg_chk_show_crypto_error $file)
		[ $cnt_kernel_traps -ne 0 ] && (echo -e "  |\t* Kernel traps: $cnt_kernel_traps"; dmesg_chk_show_kernel_traps $file)
		[ $cnt_back_trace -ne 0 ] && (echo -e "  |\t* Backtrace: $cnt_back_trace lines"; dmesg_chk_show_backtrace $file)
	done;
	[ $SHOW_FOLDER -ne 0 ] && echo ""

	cd $CURDIR
}

# $1 - Base folder for recursive check
function dmesg_summary
{
	dmesg_chk_find_log_folders $1
	[ -z "$LOG_FOLDERS" ] && return 1

	for bdir in $LOG_FOLDERS ; do
		[ ! -z "$bdir" ] && dmesg_summary_folder $bdir;
	done;
}

function dmesg_analyze
{
    cat $1 | awk \
    'BEGIN {
        c_delemiter = 0;
        c_warn = 0;
        c_print = 0;
        c_print_mask = 0;
        n_other = 0;
        n_warn = 0;
        n_oops = 0;
        n_oops_page = 0;
        n_oops_null = 0;
        n_bug = 0;
        n_wifi_authentications = 0;
        n_wifi_recovery = 0;
        n_mmc_transfer = 0;
        n_platform_pm_suspend = 0;
        n_platform_pm_suspend_alarm = 0;
        n_device_suspend_failed = 0;
        n_last_omap_reset = 0;
        n_untracked_pid = 0;
        n_lowmem_sigkill = 0;
        n_unknown_symbols = 0;
        n_wup_total = 0;
        n_wup_twl = 0;
        n_wup_wifi = 0;
        n_wup_gp_timer = 0;
        n_wup_emif = 0;
        n_wup_localtimer = 0;
        n_bind_alloc_fail = 0;
        n_pvr_shirnk_page_pool_fail = 0;
    }
    {
        if (index($0, "Resume caused by ") > 0) {
            n_wup_total++;

            if (index($0, "TWL6030") > 0) {
                n_wup_twl++;
            }

            if (index($0, "[20]") > 0) {
                n_wup_wifi++;
            }

            if (index($0, "[29]") > 0) {
                n_wup_wifi++;
            }

            if (index($0, "[7]") > 0) {
                n_wup_p7++;
            }

            if (index($0, "gp timer") > 0) {
                n_wup_gp_timer++;
            }

            if (index($0, "omap_emif") > 0) {
                n_wup_emif++;
            }

            if (index($0, "localtimer") > 0) {
                n_wup_localtimer++;
            }

            if (index($0, "prcm:") > 0) {
                n_wup_prcm++;
            }
        }

        if (index($0, "[ cut here ]") > 0) {
            c_print_mask = 1;
        }

        if (index($0, "[ end trace ") > 0) {
            c_print_mask = 0;
            c_print += 1;
        }

        if (index($0, "] WARNING: ") > 0) {
            c_print += 4;
            n_warn++;
        }

        if (index($0, "] Internal error:" ) > 0) {
            c_print += 12;
            c_print_mask = 1;
            n_oops++;
        }

        if (index($0, "] BUG: ") > 0) {
            c_print += 12;
            n_bug++;
        }

        if (index($0, "Unable to handle kernel paging request") > 0) {
            c_print += 4;
            n_oops_page++;
        }

        if (index($0, "Unable to handle kernel NULL") > 0) {
            c_print += 4;
            n_oops_null++;
        }

        if (index($0, "Kernel panic - not syncing: Fatal exception") > 0) {
           c_print += 1;
           n_panic += 1;
        }

        if (index($0, "wlan0: authenticate with") > 0) {
            c_print += 1;
            n_wifi_authentications++;
        }

        if (index($0, "ERROR watchdog interrupt received! starting recovery") > 0) {
            c_print += 1;
            n_wifi_recovery++;
        }

        if (index($0, "Hardware recovery in progress") > 0) {
            c_print += 1;
        }

        if (index($0, "error -110 transferring data") > 0) {
            c_print += 1;
            n_mmc_transfer++;
        }

        if (index($0, "pm_op(): platform_pm_suspend") > 0) {
            c_print += 1;
            n_platform_pm_suspend++;
        }

        if (index($0, "PM: Device alarm failed to suspend") > 0) {
            c_print += 1;
            n_platform_pm_suspend_alarm++;
        }

        if (index($0, "PM: Some devices failed to suspend") > 0) {
            c_print += 1;
            n_device_suspend_failed++
        }

        if (index($0, "Last OMAP reset") > 0) {
            c_print += 1;
            n_last_omap_reset++;
        }

        if (index($0, "init: untracked pid") > 0) {
            c_print += 1;
            n_untracked_pid++;
        }

        if (index($0, "send sigkill to ") > 0) {
            c_print += 1;
            n_lowmem_sigkill++;
        }

        if (index($0, "binder_alloc_buf failed to map pages in userspace, no vma") > 0) {
           c_print += 1;
           n_bind_alloc_fail++;
        }

        if (index($0, "PVR: ShrinkPagePool: Couldn") > 0) {
           c_print += 1;
           n_pvr_shirnk_page_pool_fail++;
        }

        if (index($0, "Unknown symbol") > 0) {
           c_print += 1;
           n_unknown_symbols++;
        }

        if (c_print > 0 || c_print_mask) {
            if (c_delemiter > 0) {
                c_delemiter = 0;
                print "  ---"
            }
            print $0
        } else {
            c_delemiter = 1;
        }

        if (c_print > 0) {
            c_print--;
        }
    }
    END {
        n_wup_other = n_wup_total - n_wup_twl - n_wup_wifi - n_wup_gp_timer - n_wup_emif;
        printf("====================================================================\n");
        printf("Kernel panic:                         %d\n", n_panic);
        printf("BUG:                                  %d\n", n_bug);
        printf("WARNING:                              %d\n", n_warn);
        printf("Ooops:                                %d\n", n_oops);
        printf("  - page request fails:               %d\n", n_oops_page);
        printf("  - NULL pointer:                     %d\n", n_oops_null);
        printf("WiFi authentication tries:            %d\n", n_wifi_authentications);
        printf("WiFi recovery process:                %d\n", n_wifi_recovery);
        printf("MMC data tranfer fails:               %d\n", n_mmc_transfer);
        printf("platform_pm_suspend fail:             %d\n", n_platform_pm_suspend);
        printf("  - alaram:                           %d\n", n_platform_pm_suspend_alarm);
        printf("Some devices failed to suspend:       %d\n", n_device_suspend_failed);
        printf("Unknown symbols:                      %d\n", n_unknown_symbols);
        printf("Untracked PID:                        %d\n", n_untracked_pid);
        printf("Low memory killer send SIGKILL        %d\n", n_lowmem_sigkill);
        printf("binder_alloc_buf failes               %d\n", n_bind_alloc_fail);
        printf("PVR: ShrinkPagePool                   %d\n", n_pvr_shirnk_page_pool_fail);
        printf("Resume caused by:\n");
        printf("  - IRQ 39, TWL6030-PIH               %d\n", n_wup_twl);
        printf("  - CONTROL_PADCONF_WAKEUPEVENT_0[29] %d\n", n_wup_wifi);
        printf("  - CONTROL_PADCONF_WAKEUPEVENT_1[7]  %d\n", n_wup_p7);
        printf("  - IRQ 142/143, omap_emif            %d\n", n_wup_emif);
        printf("  - IRQ 29, localtimer                %d\n", n_wup_localtimer);
        printf("  - IRQ 43, prcm                      %d\n", n_wup_prcm);
        printf("  - IRQ 69, gp timer                  %d\n", n_wup_gp_timer);
        printf("%4d|%4d|%4d|%4d|%4d|%4d|%4d|%4d|%4d|%4d\n", n_panic, n_bug, n_warn, n_oops,
            n_wup_twl, n_wup_wifi, n_wup_gp_timer, n_wup_emif, n_wup_other, n_unknown_symbols);
    }'
}

function dmesg_suspend_analyze
{
    cat $1 | awk \
    'BEGIN {
        FS = "[][]";
        prev_time = 0;
        prev_stime = 0;
        first_time = "";
        first_stime = "";
        count = 0;
        cause = "";
        printf("+----+-----------------------+---------------------------+---------------------------+-----------------------------\n");
        printf("|    |        T I M E        |           L o c a l       |         T o t a l         | \n");
        printf("| No +-------------+---------+------------+------+-------+------------+------+-------+ Resume caused by\n");
        printf("|    | Kernel time | Suspend |  Duration  |Resume|Suspend|  Duration  |Resume|Suspend| \n");
        printf("|    |    [sec]    |   [sec] |    [sec]   | [%%]  |  [%%]  |    [sec]   | [%%]  |  [%%]  | \n");
        printf("+----+-------------+---------+------------+------+-------+------------+------+-------+-----------------------------\n");
    }
    {
        curr_time = $2;
        if (!curr_time) next;

        if (first_time == "") {
            first_time = curr_time;
            prev_time = curr_time;
            next;
        };

        pos = index($0, "At Resume: ");
        if (pos > 0) {
            pos += length("At Resume: ");
            pos_end = index($0, "Pending!");
            if (pos_end <= pos)
                pos_end = length($0);
            cause = cause substr($0, pos, pos_end - pos) "; ";
            next;
        }

        pos = index($0, "Suspended for ");
        if (pos <= 0 ) next;

        count++;

        pos += length("Suspended for ");
        split(substr($0, pos), vals, " ");

        curr_stime = vals[6]
        if (first_stime == "") first_stime = curr_stime;

        loc_suspend = vals[1];
        loc_duration = curr_time - prev_time + loc_suspend;
        loc_resume = loc_duration - loc_suspend;
        loc_resume_per = (loc_resume * 100) / loc_duration;
        loc_suspend_per = (loc_suspend * 100) / loc_duration;

        tot_suspend = curr_stime - first_stime;
        tot_duration = curr_time - first_time + tot_suspend;
        tot_resume = tot_duration - tot_suspend;
        tot_resume_per = (tot_resume * 100) / tot_duration;
        tot_suspend_per = (tot_suspend * 100) / tot_duration;
        tot_drain = curr_cap - first_cap;
        tot_step = (tot_drain * 60 * 60) / tot_duration;
        printf("|%4d|%13.3f|%9.3f|%11.3f | %5.1f| %5.1f |%12.3f| %5.1f| %5.1f | %s\n",
            count-1, curr_time, loc_suspend,
            loc_duration, loc_resume_per, loc_suspend_per,
            tot_duration, tot_resume_per, tot_suspend_per, cause);

        prev_time = curr_time;
        prev_stime = curr_stime;
        cause = "";
    }
    END {
        printf("+----+-------------+---------+------------+------+-------+------------+------+-------+-----------------------------\n");
        if (tot_duration > 0) {
            tot_h = int(tot_duration / 60 / 60);
            tot_m = int((tot_duration - tot_h * 60 * 60) / 60);
            tot_s = tot_duration - tot_h * 60 * 60 - tot_m * 60;
            printf("Total:\n");
            printf("  Time:     %7d sec (%dh %dm %ds)\n", tot_duration, tot_h, tot_m, tot_s);
            printf("  Resume:   %7d sec / %7.3f %%\n", tot_resume, tot_resume_per);
            printf("  Suspend:  %7d sec / %7.3f %%\n", tot_suspend, tot_suspend_per);
            printf("Summary:|%d|%d|%d|%.2f|%.2f\n",
                tot_duration, tot_resume, tot_suspend,
                tot_resume_per, tot_suspend_per);
        } else {
            printf("Missing data or wrong format!!!\n");
        }
    }'
}

function KindleLog_analyze
{
    awk \
    'BEGIN {
        c_delemiter = 0;
        c_print = 0;
        c_print_mask = 0;
        n_wdt = 0;
        n_anr_crash = 0;
        n_zygote_exit = 0;
        n_srv_died = 0;
        n_proc_died = 0;
        n_power_cirically_low = 0;
        n_caught_shutdown = 0;
        n_shutdown_broadcast = 0;
        n_start_system_server = 0;
        n_ota_download = 0;
        n_ota_install = 0;
        n_ota_reboot = 0;
    }
    {
        if (index($0, "WATCHDOG KILLING SYSTEM PROCESS") > 0) {
            c_print++;
            n_wdt++;
        }

        if (index($0, "Crashing app skipping ANR") > 0) {
           c_print++;
           n_anr_crash++;
        }

        if (index($0, "Exit zygote because system server (") > 0) {
           c_print++;
           n_zygote_exit++;
        }

        if (index($0, " service ") > 0 && index($0, " died") > 0) {
           c_print++;
           n_srv_died++;
        }

        if (index($0, " Process ") > 0 && index($0, " has died") > 0) {
           c_print++;
           n_proc_died++;
        }

        if (index($0, "Device power is critically low, shutting down") > 0) {
            c_print++;
            n_power_cirically_low++;
        }

        if (index($0, "caught=shutdown") > 0) {
           c_print++;
           n_caught_shutdown++;
        }

        if (index($0, "powerbutton_irq") > 0) {
           c_print++;
        }

        if (index($0, "Sending shutdown broadcast") > 0) {
           c_print++;
           n_shutdown_broadcast++;
        }

        if (index($0, "Entered the Android system server!") > 0) {
           c_print++;
           n_start_system_server++;
        }

        if (index($0, ":shutdown_reason:") > 0) {
           c_print++;
        }

        if (index($0, ":ShutdownThread:") > 0) {
           c_print++;
        }

        if (index($0, "Beginning OS update installation.") > 0) {
           c_print++;
           n_ota_download++;
        }

        if (index($0, "Installing OS update") > 0) {
           c_print++;
           n_ota_install++;
        }

        if (index($0, "REBOOTING TO INSTALL") > 0) {
           c_print++;
           n_ota_reboot++;
        }

        if (c_print > 0 || c_print_mask) {
            if (c_delemiter > 0) {
                c_delemiter = 0;
                print "  ---"
            }
            print $0
        } else {
            c_delemiter = 1;
        }

        if (c_print > 0) {
            c_print--;
        }
    }
    END {
        printf("====================================================================\n");
        printf("WATCHDOG KILLING SYSTEM PROCESS:                %d\n", n_wdt);
        printf("Crashing app skipping ANR:                      %d\n", n_anr_crash);
        printf("Exit zygote because system server:              %d\n", n_zygote_exit);
        printf("Service died:                                   %d\n", n_srv_died);
        printf("Process died:                                   %d\n", n_proc_died);
        printf("Device power is critically low, shutting down:  %d\n", n_power_cirically_low);
        printf("Thermal shutdown:                               %d\n", n_caught_shutdown);
        printf("Shutdown broadcast message:                     %d\n", n_shutdown_broadcast);
        printf("Start system server:                            %d\n", n_start_system_server);
        printf("OTA:\n");
        printf("  Beginning OS update installation              %d\n", n_ota_download);
        printf("  Installing OS update                          %d\n", n_ota_install);
        printf("  REBOOTING TO INSTALL                          %d\n", n_ota_reboot);
        printf("%4d|%4d|%6d|%4d|%5d\n", n_wdt, n_anr_crash, n_zygote_exit, n_srv_died, n_caught_shutdown);
    }'
}

function KindleLog_time_indexing
{
	export IDXPREFIX="$2"
	touch ${1}_dummy
	for f in $1*; do printf "\n" >> $f; done;
	cat $1* | awk \
		'BEGIN { TYPE=ENVIRON["IDXPREFIX"]; }
		{
			LOGTIME = substr($0, 1, 18);
			LOGDATA = substr($0, 19);
			if (length(LOGDATA) == 0) next;
			printf("%s (%s%06d) %s\n", LOGTIME, TYPE, count, LOGDATA);
			count++;
		}
		END { }' > $3
	unset IDXPREFIX
}

function KindleLogs_analyze
{
	CURDIR=`pwd`
	LDIR=`readlink -e $1`
	ADIR=`readlink -e $2`
	KDIR=$LDIR/KindleLogs
	TMPDIR=$LDIR/tmp

	for i in $KDIR/*.gz ; do
		[ ! -e $i ] && continue;
		KINDLELOG=`basename $i | sed -e "s/\.gz$//"`
		KINDLELOGALL=$i
		KINDLELOGRES=${KINDLELOG}.${DIRSUM}
		echo i=$i
		echo KINDLELOGRES=$KINDLELOGRES
		echo ""
		gunzip $KINDLELOGALL -c | KindleLog_analyze > $KINDLELOGRES
	done;

	for i in $LDIR/KindleLogs/logs_*.zip ; do
		[ ! -e $i ] && continue;
		KINDLELOG=`basename $i | sed -e "s/logs/KindleLogs/" | sed -e "s/\.zip$//"`
		KINDLELOGALL=$KDIR/${KINDLELOG}.txt
		KINDLELOGRES=$ADIR/${KINDLELOG}.${DIRSUM}
		rm -fr $TMPDIR > /dev/null
		mkdir -p $TMPDIR > /dev/null
		cd $TMPDIR
		unzip $i > /dev/null

		KindleLog_time_indexing main M index.main
		KindleLog_time_indexing amazon_main A index.amazon_main
		KindleLog_time_indexing system S index.system
		KindleLog_time_indexing events E index.events
		KindleLog_time_indexing radio R index.radio
		cat index.* | sort | awk '{L1=substr($0,1,21);L2=substr($0,28);printf("%s%s\n",L1,L2)}' > $KINDLELOGALL
		rm -fr main* amazon_* system* events* radio* index.* > /dev/null
		cat $KINDLELOGALL | KindleLog_analyze > $KINDLELOGRES
		touch dummy_file
		cat * >> $KINDLELOGALL
		gzip $KINDLELOGALL > /dev/null
		rm -fr $TMPDIR > /dev/null
	done;

	cd $CURDIR;
}

function ramdump_analyze
{
	CURDIR=`pwd`
	CDIR=`readlink -e $1` # crash bin directory
	ADIR=`readlink -e $2` # analysis directory
	SDIR=`readlink -e $3` # get_logs.sh directory
	BDIR=$SDIR/../buildinfo # vmlinux directory
	RDIR=$SDIR/../ramdump # ramdump tools directory
	i=1

	cd $CDIR;
	for f in ./* ; do
		mkdir -p $ADIR/crashdump_parsed_$i
		mv $f $BDIR
		$RDIR/ramdump_parser.py -c mtk8173 -i $BDIR -o $ADIR/crashdump_parsed_$i -d
		rm -fr $BDIR/MDUMP
		mv $BDIR/$f ./
		let i=$i+1
	done;

	cd $CURDIR;
}

function get_total_s_r_count
{
	echo -n "Total S/R count: " > $2
	cat $1 | grep " \- " | awk 'BEGIN {FS = " "} { sum += $4 } END { print sum }' >> $2
}

# $1 - DEVID
function make_KindleLogs_start
{
	if [ $OPT_MANUAL_DM -ne 0 ]; then return; fi;

	echo "Starting service \";dm\"..."

	DEVID=$1
	ADBD="$ADB -s $DEVID"

	$ADBD shell rm "/sdcard/KindleLogs/*" > /dev/null 2>&1
	$ADBD shell am startservice -a com.amazon.dcp.metrics.OFFLOAD_LOGS > /dev/null
	$ADBD shell am broadcast -a "com.amazon.kindle.unifiedSearch.EasterEgg" --es "android.intent.extra.TEXT" "\;dm" > /dev/null
}

function make_KindleLogs_wait
{
	if [ $OPT_NOKINDLE_LOG -ne 0 ]; then return; fi;

	timeout=10
	SERVICENAME="Service \";dm\":"
	STATUS=""
	EXITFLAG=0
	while true; do
	    sleep 1
	    klcount=`$ADBD shell "ls -l /sdcard/KindleLogs/KindleLogs.* 2> /dev/null" | wc -l`
	    zipcount=`$ADBD shell "ls -l /sdcard/KindleLogs/*.zip 2> /dev/null" | wc -l`
	    ofcount=`$ADBD shell lsof | grep "KindleLogs/" | wc -l`
	    echo -e "$SERVICENAME Waiting ... (timeout: $timeout sec)"
	    if [ $MULTI -eq 0 ]; then echo -e "\033[2A"; fi;
	    if [ $klcount -ge 1 ]; then STATUS="Complete successful"; fi;
	    if [ $zipcount -ge 2 ]; then STATUS="Complete successful"; fi;
	    if [ $ofcount -ne 0 ]; then STATUS=""; fi;
	    if [ $timeout -eq 0 ]; then STATUS="FAIL"; fi;
	    if [ $EXITFLAG == 1 ]; then STATUS="Break"; fi;
	    if [ "$STATUS" != "" ]; then break; fi;
	    timeout=$(($timeout-1))
	done
	echo -e "\033[K$SERVICENAME \t\t ==> $STATUS <=="
}

function last_battery_status
{
	# DUMP Latest Battery status
	BATTDIR="/sys/class/power_supply/battery"
	NOW=`$ADBD shell "date +%s"| tr -cd '[:alnum:][:punct:]'`
	BATTERY_TEMP=`$ADBD shell "cat $BATTDIR/temp" | tr -cd '[:alnum:][:punct:]'`
	BATTERY_VOLTAGE_NOW=`$ADBD shell "cat $BATTDIR/voltage_now" | tr -cd '[:alnum:][:punct:]'`
	BATTERY_VOLTAGE_AVG=`$ADBD shell "cat $BATTDIR/voltage_avg" | tr -cd '[:alnum:][:punct:]'`
	BATTERY_AVG_CURRENT=`$ADBD shell "cat $BATTDIR/BatteryAverageCurrent" | tr -cd '[:alnum:][:punct:]'`
	BATTERY_CAPACITY=`$ADBD shell "cat $BATTDIR/capacity" | tr -cd '[:alnum:][:punct:]'`
	BATTERY_CHARGERVOLTAGE=`$ADBD shell "cat $BATTDIR/ChargerVoltage" | tr -cd '[:alnum:][:punct:]'`
	BATTERY_SENSEVOLTAGE=`$ADBD shell "cat $BATTDIR/BatterySenseVoltage" | tr -cd '[:alnum:][:punct:]'`
	BATTERY_ISENSEVOLTAGE=`$ADBD shell "cat $BATTDIR/ISenseVoltage" | tr -cd '[:alnum:][:punct:]'`
	BATTERY_INSTATVOLT=`$ADBD shell "cat $BATTDIR/InstatVolt" | tr -cd '[:alnum:][:punct:]'`
	BATTERY_TEMPERATURER=`$ADBD shell "cat $BATTDIR/TemperatureR" | tr -cd '[:alnum:][:punct:]'`
	BATTERY_TEMPBATTVOLTAGE=`$ADBD shell "cat $BATTDIR/TempBattVoltage" | tr -cd '[:alnum:][:punct:]'`
	printf "#time,temp,voltage_now,voltage_avg,BatteryAverageCurrent,capacity,ChargerVoltage,BatterySenseVoltage,ISenseVoltage,InstatVolt,TemperatureR,TempBattVoltage\n"
	printf "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" "$NOW" "$BATTERY_TEMP" "$BATTERY_VOLTAGE_NOW" \
		"$BATTERY_VOLTAGE_AVG" "$BATTERY_AVG_CURRENT" "$BATTERY_CAPACITY" "$BATTERY_CHARGERVOLTAGE" "$BATTERY_SENSEVOLTAGE" \
		"$BATTERY_ISENSEVOLTAGE" "$BATTERY_INSTATVOLT" "$BATTERY_TEMPERATURER" "$BATTERY_TEMPBATTVOLTAGE"
}

# $1 - DEVID
function check_build_platform
{
	dev_state=`$ADB devices 2> /dev/null | grep $1 | cut -f2`

	case "$dev_state" in
	"device") ;;
	"unauthorized")
		echo "Device $1 is unauthorized!!!. Please check ADB RSA authentication."
		return 1;;
	*)
		echo "Serial number of device $DEVID is invalid"
		return 1;;
	esac;

	ADBD="$ADB -s $1"

	BUILDNO=`$ADBD shell getprop ro.build.version.number | grep -o -E "[0-9]*"`
	BUILDTYPE=`$ADBD shell getprop ro.build.type | grep -o -E "[a-z]*"`
	BUILDPRODUCT=`$ADBD shell getprop ro.build.product | grep -o -E "[a-z]*"`

	dev_permission="root"

	case "$BUILDTYPE" in
	"user")
	   UNLOCKED=`$ADBD root`
	   if [ "$UNLOCKED"=*"adbd is already running as root"* ]; then
	        echo "Device Unlocked!!!"
	   else
		dev_permission="user"
	   fi;
		;;
	"userdebug")
		;;
	"eng")
		;;
	*)
		dev_permission="user"
		;;
	esac;

	return 0
}

# $1 - DEVID
function device_dump
{
	echo "Checking device access..."
	check_build_platform $1
	[ $? -ne 0 ] && return 1

	DEVID=$1
	ADBD="$ADB -s $DEVID"
	DIRBASEDEV=${DEVID}_${TIME}
	if [ "$OPT_BUILD_NUM" -ne 0 ]; then DIRBASEDEV=${DIRBASEDEV}_${BUILDTYPE}_${BUILDNO}_${TEST_CASE}; fi;
	if [ "$CFG_NAME" != "" ]; then DIRBASEDEV=${DIRBASEDEV}_$CFG_NAME; fi;
	dev_path[$2]=$DIRBASEDEV
	DIRBASEDEVLOCAL=$DIRBASEDEV

	DIRBASEBINDEV=$DIRBASEDEV
	DIRBASEBINDEV=$DIRBASEBIN/$DIRBASEBINDEV

	DIRBASEDEV=$DIRBASE/$DIRBASEDEV
	DIRBASESUM=$DIRBASEDEV/$DIRSUM
	DEVREADME=$DIRBASEDEV/README
	SCRLOGCUR=$DIRBASEDEV/adb_errors.log

	mkdir -p $DIRBASEDEV
	mkdir -p $DIRBASESUM

	echo "=============================="  > $DEVREADME
	echo "Date & Time     $TIME" >> $DEVREADME
	echo "Test case:      $OPT_TEST_CASE"  >> $DEVREADME
	echo "Config name:    $OPT_CONFIG_NAME" >> $DEVREADME
	echo "Options:        $OPTIONS" >> $DEVREADME
	echo "Base folder:    $DIRBASE" >> $DEVREADME
	echo "Script ver.:    $VER / $ARCH" >> $DEVREADME
	echo "User:           $USER($UID)" >> $DEVREADME
	echo "Device ID:      $DEVID" >> $DEVREADME
	echo "Build No:       $BUILDNO" >> $DEVREADME
	echo "Build Type:     $BUILDTYPE" >> $DEVREADME
	echo "Build Product:  $BUILDPRODUCT" >> $DEVREADME
	echo "Permissions:    $dev_permission" >> $DEVREADME
	echo "=============================="  >> $DEVREADME
	echo "" >> $DEVREADME

	if [ "$dev_permission" == "root" ]; then
		echo "Restarting adbd as root..."
		$ADBD root && sleep 1 && $ADBD "wait-for-device"
	fi;

	echo "Stopping service battery_log..."
	$ADBD shell "stop battery_log" 1>> $SCRLOGCUR 2>&1

	echo "Stopping service dmesg_log..."
	$ADBD shell "stop dmesg_log" 1>> $SCRLOGCUR 2>&1

	echo "Stopping service temperature_log..."
	$ADBD shell "stop temperature_log" 1>> $SCRLOGCUR 2>&1

	echo "Stopping service logcat_log..."
	$ADBD shell "stop logcat_log" 1>> $SCRLOGCUR 2>&1

	make_KindleLogs_start $DEVID

	echo $DEVID > $DIRBASEDEV/device_id.txt

	echo "Getting logcat..."
	$ADBD logcat -v time -b system -b main -b events -b amazon_main -b metrics -d > $DIRBASEDEV/logcat.log
	$ADBD logcat -v time -b kernel -d > $DIRBASEDEV/logcat.kernel.log

	if [ "$OPT_WAN" -ne 0 ]; then
		echo "Getting Radio logs..."
		$ADBD logcat -b radio -v time -d > $DIRBASEDEV/radio_logcat.log
	fi

	echo "Getting uptime..."
	$ADBD shell uptime > $DIRBASEDEV/uptime.log

	echo "Getting the product name..."
	$ADBD pull /proc/product_name $DIRBASEDEV/product_name.log 1>> $SCRLOGCUR 2>&1

	echo "Getting the interrupts..."
	$ADBD pull /proc/interrupts $DIRBASEDEV/interrupts.log 1>> $SCRLOGCUR 2>&1

	echo "Getting /proc/last_kmsg..."
	$ADBD pull /proc/last_kmsg $DIRBASEDEV/last_kmsg.log 1>> $SCRLOGCUR 2>&1

	echo "Getting BOOT Mode..."
	$ADBD pull /proc/boot_mode $DIRBASEDEV/boot_mode.log 1>> $SCRLOGCUR 2>&1

	echo "Getting Buddy info..."
	$ADBD pull /proc/buddyinfo $DIRBASEDEV/buddyinfo.log 1>> $SCRLOGCUR 2>&1

	echo "Getting CPU info..."
	$ADBD pull /proc/cpuinfo $DIRBASEDEV/cpuinfo.log 1>> $SCRLOGCUR 2>&1

	echo "Getting DRAM info..."
	$ADBD pull /proc/draminfo $DIRBASEDEV/draminfo.log 1>> $SCRLOGCUR 2>&1

	echo "Getting MMC vendor info..."
	$ADBD pull /proc/emmcinfo $DIRBASEDEV/emmcinfo.log 1>> $SCRLOGCUR 2>&1

	echo "Getting MMC size info..."
	$ADBD pull /proc/emmcsize $DIRBASEDEV/emmcsize.log 1>> $SCRLOGCUR 2>&1

	echo "Getting MEM info..."
	$ADBD pull /proc/meminfo $DIRBASEDEV/meminfo.log 1>> $SCRLOGCUR 2>&1

	echo "Getting page type info..."
	$ADBD pull /proc/pagetypeinfo $DIRBASEDEV/pagetypeinfo.log 1>> $SCRLOGCUR 2>&1

	echo "Getting slab info..."
	$ADBD pull /proc/slabinfo $DIRBASEDEV/slabinfo.log 1>> $SCRLOGCUR 2>&1

	echo "Getting VM Alloc info..."
	$ADBD pull /proc/vmallocinfo $DIRBASEDEV/vmallocinfo.log 1>> $SCRLOGCUR 2>&1

	echo "Getting VM Status..."
	$ADBD pull /proc/vmstat $DIRBASEDEV/vmstat.log 1>> $SCRLOGCUR 2>&1

	echo "Getting zone info..."
	$ADBD pull /proc/zoneinfo $DIRBASEDEV/zoneinfo.log 1>> $SCRLOGCUR 2>&1

	echo "Getting device model..."
	$ADBD pull /proc/devmodel $DIRBASEDEV/devmodel.log 1>> $SCRLOGCUR 2>&1

	echo "Getting average load..."
	$ADBD pull /proc/loadavg $DIRBASEDEV/loadavg.log 1>> $SCRLOGCUR 2>&1

	echo "Getting disk status..."
	$ADBD pull /proc/diskstats $DIRBASEDEV/diskstats.log 1>> $SCRLOGCUR 2>&1

	echo "Getting timer list..."
	$ADBD pull /proc/timer_list $DIRBASEDEV/timer_list.log 1>> $SCRLOGCUR 2>&1

	echo "Getting mount info..."
	$ADBD shell mount > $DIRBASEDEV/mount.log

	echo "Capturing the latest battery status, since we stopped the battery_log service"
	last_battery_status >> $DIRBASEDEV/battery_current.log

	echo "Getting current dmesg log..."
	$ADBD shell dmesg > $DIRBASEDEV/dmesg_current.log

	echo "Getting lsof logs..."
	$ADBD shell lsof > $DIRBASEDEV/lsof.log

	echo "Getting WiFi logs..."
	$ADBD pull /data/misc/wifi/fwlogs $DIRBASEDEV/wifi_logs 1>> $SCRLOGCUR 2>&1

	echo "Getting audio state..."
	$ADBD pull /proc/asound $DIRBASEDEV/asound 1>> $SCRLOGCUR 2>&1

	echo "Getting tinymix output..."
	$ADBD shell tinymix > $DIRBASEDEV/tinymix.log

	echo "Getting audio register dump..."
	$ADBD shell cat /sys/kernel/debug/mtksocaudio > $DIRBASEDEV/mtksocaudio.log
	$ADBD shell cat /sys/kernel/debug/mtksocdlaudio > $DIRBASEDEV/mtksocdlaudio.log

	echo "Getting h2w state..."
	$ADBD shell cat /sys/class/switch/h2w/state > $DIRBASEDEV/h2w_state.log

	if [ "$dev_permission" == "root" ]; then
		echo "Getting dropbox..."
		$ADBD pull /data/system/dropbox $DIRBASEDEV/dropbox 1>> $SCRLOGCUR 2>&1

		echo "Getting logmanager..."
		$ADBD pull /data/data/com.amazon.device.logmanager/files/ $DIRBASEDEV/dropbox 1>> $SCRLOGCUR 2>&1

		# we do not need to routinely collect strictmode violations
		rm $DIRBASEDEV/dropbox/system_app_strictmode@* 2>/dev/null

		echo "Getting debug_service logs..."
		$ADBD pull $BASELOGPATH $DIRBASEDEV/debug_service 1>> $SCRLOGCUR 2>&1

		echo "Getting cached dropbox..."
		$ADBD pull /cache/data/system/dropbox $DIRBASEDEV/cache_dropbox 1>> $SCRLOGCUR 2>&1

		echo "Getting cached debug_serivce..."
		$ADBD pull /cache/$BASELOGPATH $DIRBASEDEV/cache_debug_service 1>> $SCRLOGCUR 2>&1

		echo "Getting cached recovery log..."
		$ADBD pull /cache/recovery $DIRBASEDEV/recovery 1>> $SCRLOGCUR 2>&1

		if [ $OPT_CLEAN_LOGS -eq 1 ]; then
			echo "Clean local logs..."
			$ADBD shell rm -rf /data/system/dropbox/*
			$ADBD shell rm -rf $BASELOGPATH/archive/*
			$ADBD shell rm -rf $BASELOGPATH/last_kmsg*
			$ADBD shell rm -rf /cache/data/system/dropbox/*
			$ADBD shell rm -rf /cache/$BASELOGPATH/*
		fi
	fi;

	WORK_DIR=$(dirname $(pwd))
	WORK_DIR=`basename $WORK_DIR`
	WORK_DIR=${WORK_DIR##*-}
	INCREMENTAL=`$ADBD shell getprop ro.build.version.incremental |tr -d '\015'`
	if [ "$INCREMENTAL" = "$WORK_DIR" ]; then
		echo "Getting ramdump..."
		$ADBD pull /data/crashes $DIRBASEBINDEV 1>> $SCRLOGCUR 2>&1
	else
		echo "****************************************************************************************************"
		echo "*Warning, you must run this script in correct place, or the ramdump parsed result will be incorrect*"
		echo -e "Skip step getting ramdump because of incorrect work place, please follow the steps to recollect ramdump\n1.Download the correct image from KBITS,current device vesion is $INCREMENTAL\n2.Uncompress the image files,do not change any file name and location\n3.Run this script under the folder scripts" > ramdump_parser_skipped.txt
		echo "*Skip getting ramdump                                                                              *"
		echo "*Look ramdump_parser_skipped.txt for more detail                                                   *"
		echo "****************************************************************************************************"
	fi

	echo "Getting tombstones..."
	$ADBD pull /data/tombstones $DIRBASEDEV/tombstones 1>> $SCRLOGCUR 2>&1

	echo "Getting ANR traces..."
	$ADBD pull /data/anr $DIRBASEDEV/anr 1>> $SCRLOGCUR 2>&1

	echo "Getting GPU traces..."
	$ADBD pull /d/sync $DIRBASEDEV 1>> $SCRLOGCUR 2>&1
	$ADBD pull /d/pvr/debug_dump $DIRBASEDEV 1>> $SCRLOGCUR 2>&1

	echo "Getting persistent traces..."
	$ADBD pull /d/persistent_trace $DIRBASEDEV/persistent_trace.log 1>> $SCRLOGCUR 2>&1

	echo "Getting wakeup_sources..."
	$ADBD pull /d/wakeup_sources $DIRBASEDEV/wakeup_sources.log 1>> $SCRLOGCUR 2>&1

	echo "Getting wakeup_events..."
	$ADBD pull /d/wakeup_events $DIRBASEDEV/wakeup_events.log 1>> $SCRLOGCUR 2>&1

	echo "Getting wow_events..."
	$ADBD pull /d/wow_events $DIRBASEDEV/wow_events.log 1>> $SCRLOGCUR 2>&1

	echo "Getting /d/suspend_time..."
	$ADBD pull /d/suspend_time $DIRBASEDEV/dbg_suspend_time.log 1>> $SCRLOGCUR 2>&1

	echo "Getting /d/pm_debug/count..."
	$ADBD pull /d/pm_debug/count $DIRBASEDEV/dbg_pm_debug_count.log 1>> $SCRLOGCUR 2>&1

	echo "Getting process status..."
	$ADBD shell ps -t -x -P -p -c > $DIRBASEDEV/process.log

	echo "Getting system properties..."
	$ADBD shell getprop > $DIRBASEDEV/properties.log

	echo "Getting disk usage status..."
	$ADBD shell df > $DIRBASEDEV/disk_usage.log

	echo "Getting /d/suspend_stats..."
	$ADBD pull /d/suspend_stats $DIRBASEDEV/suspend_stats.log 1>> $SCRLOGCUR 2>&1

	if [ "$dev_permission" == "root" ]; then
		echo "Getting core dump files"
		$ADBD pull /data/core_dump/ $DIRBASEDEV/ 1>> $SCRLOGCUR 2>&1

		echo "Getting ETM analysis..."
		$ADBD pull /data/SYS_ETM_RESULT* $DIRBASEDEV/ 1>> $SCRLOGCUR 2>&1
	fi;

	echo "Getting suspend resume log..."
	mkdir -p $DIRBASEDEV/suspend_resume
	$ADBD logcat -b amazon_main -d | grep AmazonSuspendResume > $DIRBASEDEV/suspend_resume/suspend_logcat.log
	$ADBD pull /data/data/com.amazon.suspendresume/files $DIRBASEDEV/suspend_resume/suspend_apk_log/ 1>> $SCRLOGCUR 2>&1

	if [ "$OPT_WAN" -ne 0 ]; then
		echo "Getting MODEM info..."
		echo "Running COMMAND - wankit atcmd -c ati -v -o" >> $DIRBASEDEV/modeminfo.txt
		$ADBD shell wankit atcmd -c ati -v -o >> $DIRBASEDEV/modeminfo.txt
		echo "" >> $DIRBASEDEV/modeminfo.txt
		echo "Running COMMAND - netcfg to check IP" >> $DIRBASEDEV/modeminfo.txt
		$ADBD shell netcfg >> $DIRBASEDEV/modeminfo.txt
		echo "" >> $DIRBASEDEV/modeminfo.txt
		echo "Checking if USB ports are available" >> $DIRBASEDEV/modeminfo.txt
		$ADBD shell ls /dev/ttyUSB* >> $DIRBASEDEV/modeminfo.txt
		echo "" >> $DIRBASEDEV/modeminfo.txt
		echo "Displaying output of cat /d/usb/devices" >> $DIRBASEDEV/modeminfo.txt
		$ADBD shell cat /d/usb/devices >> $DIRBASEDEV/modeminfo.txt
		echo "" >> $DIRBASEDEV/modeminfo.txt
	fi

	echo "Getting bugreport..."
	$ADBD shell bugreport > $DIRBASEDEV/bugreport.log

	echo "Getting dumpsys..."
	$ADBD shell dumpsys > $DIRBASEDEV/dumpsys.log

#	make_KindleLogs_wait $DEVID

#	echo "Getting KindelLogs..."
#	$ADBD pull /sdcard/KindleLogs $DIRBASEDEV/KindleLogs 1>> $SCRLOGCUR 2>&1

	if [ "$WORK_DIR" == "$INCREMENTAL" ]; then
		if [ -d "$DIRBASEBINDEV" ]; then
			echo "Analyze ramdump log..."
			ramdump_analyze $DIRBASEBINDEV $DIRBASESUM $DIRSCRIPT
		fi;
	fi

	if [ -f "$DIRBASEDEV/battery.log" ]; then
	    echo "Analyze battery log..."
	    battery_analyze $DIRBASEDEV/battery.log > $DIRBASESUM/battery.$DIRSUM
	fi

	if [ -f "$DIRBASEDEV/dmesg_full.log" ]; then
	    echo "Analyze dmesg_full log..."
	    dmesg_analyze $DIRBASEDEV/dmesg_full.log > $DIRBASESUM/dmesg_full.$DIRSUM
	    dmesg_suspend_analyze $DIRBASEDEV/dmesg_full.log > $DIRBASESUM/dmesg_suspend.$DIRSUM
	fi

	if [ -f "$DIRBASEDEV/last_kmsg.log" ]; then
	    echo "Analyze last_kmsg log..."
	    dmesg_analyze $DIRBASEDEV/last_kmsg.log > $DIRBASESUM/last_kmsg.$DIRSUM
	fi

	if [ -f "$DIRBASEDEV/dbg_suspend_time.log" ]; then
	    echo "Analyze suspend_time..."
	    get_total_s_r_count $DIRBASEDEV/dbg_suspend_time.log $DIRBASESUM/total_suspends.txt
	fi

	if [ -d "$DIRBASEDEV/KindleLogs" ]; then
	    echo "Analyze Kindle logs..."
	    KindleLogs_analyze $DIRBASEDEV $DIRBASESUM
	fi

	make_table $DIRBASEDEVLOCAL >> $DEVREADME

	cd $DIRBASE/..
	if [ $OPT_ARCHIVE -ne 0 ]; then
	    echo "Creating archive file ${DIRBASE}.zip"
	    zip -s 24M -r9 ${DIRBASE}.zip $DIRBASE

		if [ "$WORK_DIR" == "$INCREMENTAL" ]; then
			if [ -d $DIRBASEBIN ]; then
				echo "Creating archive file ${DIRBASEBIN}.zip"
				zip -s 500M -r9 ${DIRBASEBIN}.zip $DIRBASEBIN
			fi;
		fi;
	fi;

	echo "Done!"

	sleep 1
}

function make_base
{
	## reset

	mkdir -p $DIRBASE
	if [ "$?" -ne "0" ]; then echo "Can't make base folder: $DIRBASE"; exit -1; fi
	echo "=============================="  | tee -a $README
	echo "Date & Time  $TIME" | tee -a $README
	echo "Test case:   $OPT_TEST_CASE"  | tee -a $README
	echo "Config name: $OPT_CONFIG_NAME" | tee -a $README
	echo "Options:     $OPTIONS" | tee -a $README
	echo "Base folder: $DIRBASE" | tee -a $README
	echo "Script ver.: $VER / $ARCH" | tee -a $README
	echo "OS:          $OSVER" | tee -a $README
	echo "Bash ver.:   $BASH_VERSION" | tee -a $README
	echo "User:        $USER($UID)" | tee -a $README

	dev_list=(`$ADB devices | grep -E 'unauthorized$|device$' | cut -f 1`)
	dev_count=${#dev_list[*]}
	HDRVERT=$(($dev_count+11))

	if [ "$OPT_DEVICE_NUM" != "" ]; then
		SELECT=0
		for dev in ${dev_list[*]}; do [ "$OPT_DEVICE_NUM" == "$dev" ] && SELECT=1; done
		dev_count=$SELECT
		unset dev_list
		if [ $SELECT -eq 0 ]; then
			echo "Selected device \"$OPT_DEVICE_NUM\" is not connected" | tee -a $README
		else
			dev_list[0]=$OPT_DEVICE_NUM
		fi;
	else
		SELECT=$dev_count
	fi;

	echo "Devices:" | tee -a $README
	for dev in ${dev_list[*]}; do echo "   $dev"; done  | tee -a $README
	echo "==============================" | tee -a $README

	[ $SELECT -eq 0 ] && exit 1;
}

function multi_logs
{
	MULTI=1

	for i in `seq 0 $(($dev_count-1))`; do
	    DEVID=${dev_list[$i]}
	    y=$(($HDRVERT+2+$i))
	    echo -e "\033[$y;0H$DEVID: "
	    coproc device_dump $DEVID $i
	    y=$(($HDRVERT+3+$i))
	    echo -e -n "\033[$y;0H\033[J"
	    dev_in[$i]=${COPROC[0]}
	    dev_out[$i]=${COPROC[1]}
	    dev_pid[$i]=$COPROC_PID
	done

	while true; do
	    process=0
	    for i in `seq 0 $(($dev_count-1))`; do
		if [ ${dev_pid[$i]} -eq -1 ]; then continue; fi
		process=1
		PIPE=/proc/$BASHPID/fd/${dev_in[$i]}
		read -t0.1 msg 2>/dev/null 2>/dev/null < $PIPE
		err=$?
		y=$(($HDRVERT+2+$i))
		case $err in
		  0) echo -e -n "\033[$y;19H$msg\033[K\033[$((HDRVERT+2+$dev_count));0H";;
		  1) dev_pid[$i]=-1;;
		142);;
		  *) echo "unknown error $err on dev=${dev_list[$i]}";;
		esac
	    done;
	    if [ $process -eq 0 ]; then break; fi
	done;
}

function make_table
{
	echo ""
	echo "Summary of dropbox:"
	echo "+----------------+------------------------------------------------------------------------------+-------+"
	echo "|   Device ID    |                              P R O C E S S                                   | Count |"
	echo "+----------------+------------------------------------------------------------------------------+-------+"
	for path in $@; do
	    DIRBASEDEV=$DIRBASE/$path
	    DIRDROPBOX=$DIRBASEDEV/dropbox
	    DEVID=`cat $DIRBASEDEV/device_id.txt`

	    [ ! -d $DIRDROPBOX ] && continue;
	    printf "|%-16s|                                                                              |       |\n" "$DEVID"

	    count=`find $DIRDROPBOX -name "system_app_anr*.gz" | wc -l`
	    if [ $count -ne 0 ]; then
		LSTPROCESS=`gunzip $DIRDROPBOX/system_app_anr*.gz -c 2>/dev/null | grep "^Process: " | sed -e "s/^Process: //" | sort -u`
		for proc in $LSTPROCESS ; do
		    count=`gunzip $DIRDROPBOX/system_app_anr*.gz -c 2>/dev/null | grep " $proc" --count`
		    printf "|    App ANR     | %-77s|%6d |\n" "$proc" "$count"
		done;
	    fi;

	    count=`find $DIRDROPBOX -name "system_app_crash*.gz" | wc -l`
	    if [ "$count" -ne 0 ]; then
		LSTPROCESS=`gunzip $DIRDROPBOX/system_app_crash*.gz -c 2>/dev/null | grep "^Process: " | sed -e "s/^Process: //" | sort -u`
		for proc in $LSTPROCESS ; do
		    count=`gunzip $DIRDROPBOX/system_app_crash*.gz -c 2>/dev/null | grep " $proc" --count`
		    printf "|    App Crash   | %-77s|%6d |\n" "$proc" "$count"
		done;
	    fi;

	    count=`find $DIRDROPBOX -name "system_app_strictmode*.gz" | wc -l`
	    if [ "$count" -ne 0 ]; then
		LSTPROCESS=`gunzip $DIRDROPBOX/system_app_strictmode*.gz -c 2>/dev/null | grep "^Process: " | sed -e "s/^Process: //" | sort -u`
		for proc in $LSTPROCESS ; do
		    count=`gunzip $DIRDROPBOX/system_app_strictmode*.gz -c 2>/dev/null | grep " $proc" --count`
		    printf "|    Strict mode | %-77s|%6d |\n" "$proc" "$count"
		done;
	    fi;

	    count=`find $DIRDROPBOX -name "SYSTEM_TOMBSTONE*.gz" | wc -l`
	    if [ "$count" -ne 0 ]; then
		LSTPROCESS=`gunzip $DIRDROPBOX/SYSTEM_TOMBSTONE*.gz -c 2>/dev/null | \
			grep -o ">>> [a-zA-Z0-9\/\-\_]* <<<" | sed -e "s/>>> //" | sed -e "s/ <<<//" | sort -u`
		for proc in $LSTPROCESS ; do
		    count=`gunzip $DIRDROPBOX/SYSTEM_TOMBSTONE*.gz -c 2>/dev/null | grep ">>> $proc <<<" --count`
		    printf "|    Tombstone   | %-77s|%6d |\n" "$proc" "$count"
		done;
	    fi;
	done;
	echo "+----------------+------------------------------------------------------------------------------+-------+"

	echo ""
	echo "Summary of all dmesg and last_kmsg files:"
	for path in $@; do echo "$DIRBASE/$path"; dmesg_summary_folder $DIRBASE/$path; done;
}

function mode1_get_logs
{
	$ADB version > /dev/null 2>&1
	if [ "$?" -ne "0" ]; then
	  echo "Path to adb tool is wrong!!!";
	  return;
	fi;

	CFG_NAME=`echo "$OPT_CONFIG_NAME" | sed -e "s/[ !@#$%^&*]/_/g"`
	TEST_CASE=`echo "$OPT_TEST_CASE" | sed -e "s/[ !@#$%^&*]/_/g"`
	TIME=`date +%Y%m%d_%H%M`
	DIRBASE=`printf "%s_%s" "$TIME" "$TEST_CASE"`
	DIRBASEBIN=$DIRBASE"_crashbin"
	DIRSCRIPT=$OPT_SCRIPT_DIR
	COMMONREADME=$DIRBASE/README;
	OSVER=`lsb_release -a 2>/dev/null | grep Description | sed -e "s/Description:[ \t]//"`
	DIRSUM=analysis
	MULTI=0
	OPTIONS=""
	if [ $OPT_MANUAL_DM -ne 0 ]; then OPTIONS+="--manual_dm "; fi
	if [ $OPT_ARCHIVE -eq 1 ]; then OPTIONS+="--archive "; fi;
	if [ $OPT_NOKINDLE_LOG -ne 0 ]; then OPTIONS+="--nokindlelog "; fi;
	if [ "$OPT_DEVICE_NUM" != "" ]; then OPTIONS+="--Device $OPT_DEVICE_NUM "; fi;

	make_base;

	case "$dev_count" in
	    "0") echo "No found device(s)"; exit -1;;
	    "1") device_dump ${dev_list[0]} 0 ;;
	    *) multi_logs;;
	esac;
}

function enable_all_log_services
{
	if [ "$1" == "" ]; then
	    dev_list=(`$ADB devices | grep 'device$' | cut -c 1-16`)
	    for dev in $dev_list; do enable_all_log_services "$dev"; done;
	else
	    ADBDEV="$ADB -s $1"

	    $ADBDEV root
	    $ADBDEV wait-for-device
	    $ADBDEV remount

	    echo "Set core dump path"
	    $ADBDEV shell 'mkdir /data/core_dump'
	    $ADBDEV shell 'echo /data/core_dump/core_%e.%p > /proc/sys/kernel/core_pattern'

	    echo "Start-up service battery_log..."
	    $ADBDEV shell 'echo debug.log.battery.enable=y >> /system/build.prop'
	    $ADBDEV shell 'start battery_log'

	    echo "Start-up service temperature log..."
	    $ADBDEV shell 'echo debug.log.temperature.enable=y >> /system/build.prop'
	    $ADBDEV shell 'start temperature_log'

	    echo "Start-up service dmesg_log"
	    $ADBDEV shell 'echo debug.log.dmesg.enable=y >> /system/build.prop'
	    $ADBDEV shell 'start dmesg_log'

	    echo "Start-up service logcat_log"
	    $ADBDEV shell 'echo debug.log.logcat.enable=y >> /system/build.prop'
	    $ADBDEV shell 'start logcat_log'

	    $ADBDEV shell sync
	fi;
}

function mode2_battery_log
{
	battery_analyze $OPT_CHK_BATTERY
}

function mode3_dmesg_log
{
	dmesg_analyze $OPT_CHK_DMESG
}

function mode4_kindle_log
{
	cat $OPT_CHK_KINDLELOG | KindleLog_analyze
}

function mode5_dmesg_suspend_log
{
	dmesg_suspend_analyze $OPT_CHK_DMESG_SUSPEND
}

function mode6_start_log_Services
{
	enable_all_log_services
}

function exec_modes
{
	case $RUN_MODE in
	0) mode0_help;;
	1) mode1_get_logs;;
	2) mode2_battery_log;;
	3) mode3_dmesg_log;;
	4) mode4_kindle_log;;
	5) mode5_dmesg_suspend_log;;
	6) mode6_start_log_Services;;
	*) echo "Unknown mode type ($RUN_MODE):";;
	esac;
}

arg_parser "$0" "$1" "$2" "$3" "$4" "$5" "$6" "$7" "$8" "$9"
arg_check;
exec_modes;
