# python3 sensorassist.py for ZB house(2 config) sensor testing by Alec Belsher
	# How to use:
	# copy and paste Smart Home Utility device list into a text file names sensorfound.txt
	# with the .txt in same directory, it will print out a picture of connected devices denoted by an X
	# Overwrite the file with device lists until all sensors are found

from __future__ import print_function
import math
import re
import importlib


# change this for no. of items per line
n = 5

# device MAC's
dev1 = '24:FD:5B:00:01:08:87:85' 
dev2 = '24:FD:5B:00:01:08:FB:49'
dev3 = '24:FD:5B:00:01:08:C7:E8'
dev4 = '24:FD:5B:00:01:08:FB:50'
dev5 = '24:FD:5B:00:01:08:EA:FD'
dev6 = '24:FD:5B:00:01:08:E8:DA'
dev7 = '24:FD:5B:00:01:00:7B:CF'
dev8 = '24:FD:5B:00:01:08:C8:6D'
dev9 = '24:FD:5B:00:01:08:C9:68'
dev10 = '24:FD:5B:00:01:08:BD:67'
dev11 = '24:FD:5B:00:01:08:C9:1F'
dev12 = '24:FD:5B:00:01:08:C7:C6'
dev13 = '24:FD:5B:00:01:08:E9:D1'
dev14 = '24:FD:5B:00:01:0B:22:31'
dev15 = '24:FD:5B:00:01:08:E8:7F'
dev16 = '24:FD:5B:00:01:08:FB:4D'
dev16 = '24:FD:5B:00:01:0B:2B:5E'
dev17 = '24:FD:5B:00:01:08:FB:7D'
dev18 = '24:FD:5B:00:01:0B:22:9B'
dev19 = '24:FD:5B:00:01:08:C7:E3'
dev20 = '24:FD:5B:00:01:08:FB:7E'
dev21 = '24:FD:5B:00:01:08:EB:22'
dev22 = '24:FD:5B:00:01:0B:0D:3E'
dev23 = '24:FD:5B:00:01:08:C8:6C'
dev24 = '24:FD:5B:00:01:0B:0C:9A'
dev25 = '24:FD:5B:00:01:08:E9:4F'


# must reflect above variables in set
devices = [
    dev1, dev2, dev3, dev4, dev5, dev6, dev7, dev8, dev9, dev10, dev11, dev12, dev13, dev14, dev15, dev16, dev17, dev18, dev19, dev20, dev21, dev22, dev23, dev24, dev25,
]


# leave these blank
lines = []

macs = []

picture = []


# open file with SHU return
with open('sensorfound.txt', 'r') as fil:
	for line in fil.readlines():
		a = re.search(r'([0-9A-F]{2}[:-]){7}([0-9A-F]{2})', line, re.I)
		lines.append(a)
	fil.close()


# compare devices MAC with lists MAC
for i in devices:
	if i in str(lines):
		picture.append("X")
	else:
		picture.append("_")


# print a cool picture
for x in (picture[i:i + n] for i in range(0, len(picture), n)):
	print(" ".join(x))
