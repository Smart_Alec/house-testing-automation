#!/bin/bash
# ./autoADB.sh - ADB command tool by Alec Belsher

# Examples
# ./autoADB.sh reboot
# ./autoADB.sh push <file>
# ./autoADB.sh version
# ./autoADB.sh install api.apk
# ./autoADB.sh uninstall com.example.android.apis

adb devices | while read line
do
    if [ ! "$line" = "" ] && [ `echo $line | awk '{print $2}'` = "device" ]
    then
        device=`echo $line | awk '{print $1}'`
        echo "$device $@ ..."
        adb -s $device $@
    fi
done
