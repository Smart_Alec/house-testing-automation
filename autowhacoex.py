# python3 auto(mated)whacoex.py v1.6 by Alec Belsher for Allion USA
	# must have a get_logs.sh on userDirectory named get_logs_reference.sh


import os
import subprocess
from shutil import copyfile
import collections
import time
import multiprocessing

def main():
# Locations: Must reflect how many locations are being used
	L1 = 'LR'
	L2 = 'DR'
	L3 = 'B2'
	L4 = 'B3'

# Location list
	LX = (L1, L2, L3, L4,)

# DSN @ locations: Must reflect how many devices are being used
# Current DUT: Bishop EVT
#	L1DSN = 'G000RA03815209EU'
#	L2DSN = 'G000RA03815209BM'
#	L3DSN = 'G000RA0381240857'
#	L4DSN = 'G000RA0381240538'

# Current DUT: Donut DVT
#	L1DSN = 'G0B0VC0582750039'
#	L2DSN = 'G0B0VC0582750031'
#	L3DSN = 'G0B0U6058272003W'
#	L4DSN = 'G0B0U60582720011'

#Current DUT: Lidar DVT
	L1DSN = 'G2A0WK04828400BU'
	L2DSN = 'G2A0V704828300C9'
	L3DSN = 'G2A0U20482740019'
	L4DSN = 'G2A0U2048274000G'

# DSN list
	DSN = (L1DSN, L2DSN, L3DSN, L4DSN,) 

# Dictionary: Make sure the correct Location is paired to the correct DSN
	pairs = {L1DSN:L1,L2DSN:L2,L3DSN:L3,L4DSN:L4,}
	pairs = collections.OrderedDict(pairs)

# Configuration: Change depending on needs / user
	userDirectory = '/home/allion/Desktop/'
	zigbeepath = '/data/misc/zigbee/'
	zigbeepath2 = '/data/system/'
	targetFolder = 'Test_files/'
	if os.path.exists(targetFolder):
		os.rmdir(targetFolder)
	targetDirectory = userDirectory + targetFolder
	os.makedirs(targetFolder)

############################################################
	coex = False
# change ^^^ to True or False
	coex_folder = 'ZB_details'
	if coex == True:
		print("Coex = True\n")
		if os.path.exists(coex_folder):
			os.rmdir(coex_folder)
		os.makedirs(coex_folder)
	else:
		print("Coex = False\n")
############################################################

# generate folders
	folder_name = input('What do you want to name this folder?: ')
	while os.path.exists(userDirectory + folder_name):
		folder_name = input('What do you want to name this folder?: ')
	os.makedirs(folder_name)

# root and remount
	for i in DSN:
		subprocess.call("adb -s {} root && adb -s {} remount".format(i,i), shell = True)

# initialize logcat in multiprocess - !!! Replace DSN's and comment out unused locations !!!
	def L1logcat():
		cmdastring = str('adb -s ' + L1DSN + ' shell logcat -v time')
		cmda = cmdastring.split()
		proca = subprocess.Popen(cmda, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		for line in proca.stdout:
			with open('LRlogcat.log', 'a') as output:
				output.write(line.decode('utf-8'))

	def L2logcat():
		cmdbstring = str('adb -s ' + L2DSN + ' shell logcat -v time')
		cmdb = cmdbstring.split()
		procb = subprocess.Popen(cmdb, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		for line in procb.stdout:
			with open('DRlogcat.log', 'a') as output:
				output.write(line.decode('utf-8'))

	def L3logcat():
		cmdcstring = str('adb -s ' + L3DSN + ' shell logcat -v time')
		cmdc = cmdcstring.split()
		procc = subprocess.Popen(cmdc, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		for line in procc.stdout:
			with open('B2logcat.log', 'a') as output:
				output.write(line.decode('utf-8'))

	def L4logcat():
		cmddstring = str('adb -s ' + L4DSN + ' shell logcat -v time')
		cmdd = cmddstring.split()
		procd = subprocess.Popen(cmdd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		for line in procd.stdout:
			with open('B3logcat.log', 'a') as output:
				output.write(line.decode('utf-8'))

	a = multiprocessing.Process (target=L1logcat)
	a.start()
	b = multiprocessing.Process (target=L2logcat)
	b.start()
	c = multiprocessing.Process (target=L3logcat)
	c.start()
	d = multiprocessing.Process (target=L4logcat)
	d.start()

# !!! DO NOT TOUCH ANYTHING BELOW THIS COMMENT !!!

	input('Press Enter to gather logs and reboot')

	if coex == True:
		os.chdir(userDirectory + coex_folder)
		for k, v in pairs.items():
			subprocess.call("adb -s " + k + " pull " + zigbeepath + "device_database.dat device_database_" + v + ".dat", shell=True)
			subprocess.call("adb -s " + k + " pull " + zigbeepath + "zigbee_adapter.txt zigbee_adapter_" + v + ".txt", shell=True)
			subprocess.call("adb -s " + k + " pull " + zigbeepath + "zigbee_fw.xml zigbee_fw_" + v + ".xml", shell=True)
			subprocess.call("adb -s " + k + " pull " + zigbeepath2 + "zigbee.db zigbee.db_" + v + ".db", shell=True)
		os.chdir(userDirectory)

	if os.path.exists('DUT_logs/'):
		os.rmdir('DUT_logs/')
	os.makedirs('DUT_logs/')
	copyfile(userDirectory + "get_logs_reference.sh", userDirectory + 'DUT_logs/' + 'get_logs.sh')
	os.chdir(userDirectory + "DUT_logs")
	subprocess.call("chmod +x get_logs.sh", shell = True)

	process = subprocess.Popen(["./get_logs.sh"])
	process.wait()

	for k, v in pairs.items():
		subprocess.call("adb -s " + k + " shell wpa_cli -i wlan0 -p /data/misc/wifi/sockets signal_poll > " + v + "signalinfo.txt && adb -s " + k + " shell wpa_cli -i wlan0 -p /data/misc/wifi/sockets status > " + v + "statusinfo.txt", shell=True)
	os.chdir(userDirectory)

# close logcat multithread
	a.terminate()
	a.join()
	b.terminate()
	b.join()
	c.terminate()
	c.join()
	d.terminate()
	d.join()
	
# clear logcat
	for i in DSN:
		subprocess.call("adb -s {} shell logcat -c".format(i), shell = True)

# reboot
	for i in DSN:
		subprocess.call("adb -s {} shell reboot".format(i), shell = True)

# collect
	for j in LX:
		if os.path.exists(j + 'logcat.log'):
			os.rename(userDirectory + j + "logcat.log", targetDirectory + j + "logcat.log")

	os.rename(userDirectory + "DUT_logs", targetDirectory + "DUT_logs")
	if coex == True:
		os.rename(userDirectory + "ZB_details", targetDirectory + "ZB_details")
		subprocess.call('mv ' + userDirectory + '*.log ' + targetDirectory, shell = True)

	os.rename(targetDirectory, userDirectory + folder_name)

main()
